

puts "Sums by size with counts"
(1..9).each do |size|

  min = (1..size).sum
  max = ((9-size+1)..9).sum
  puts "#{size}: #{min}-#{max} (#{max-min+1})"
end
