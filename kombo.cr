

if ARGV.size != 2
  puts "Usage: kombo <number> <spaces>"
  exit
end

MAX_SIZE = 9
MAX_VALUE = 9

run = maxRun(ARGV[0].to_i, ARGV[1].to_i)
puts run
puts run.sum


# Return the max run that is still under the target num
def maxRun(num, size)
  if size > MAX_SIZE
    return [] of Int32
  end

  if (0..size).sum > num
    return [] of Int32
  end

  top = MAX_VALUE
  run = (top-size+1)..top

  while run.sum > num
    top -= 1
    run = (top-size+1)..(top)
  end

  return run.to_a
end
